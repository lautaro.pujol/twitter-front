import React, { ChangeEvent, Component } from 'react'
import TweetList from './tweetList'
import styles from './style.module.css'
import UserActions from '../../actions/user.actions'

type Props = {
    nickname: string
}

type State = {
    foundUserNickname: string,
    searchInput: string,
    follow: (nicknameToFollow: string, followerNickname: string) => Promise<void>
}

export default class UserSearchComponent extends Component<Props, State> {
    followHandler() {
        this.state.follow(this.state.foundUserNickname, this.props.nickname)
    }
    searchInputHandler(event: ChangeEvent<HTMLInputElement>) {
        this.setState({ searchInput: event.target.value })
    }
    searchHandler() {
        this.setState((prevState) => ({ foundUserNickname: prevState.searchInput }))
    }
    constructor(props: Props) {
        super(props);
        this.state = {
            foundUserNickname: '',
            searchInput: '',
            follow: new UserActions().addFollower
        };
        this.searchHandler = this.searchHandler.bind(this)
        this.searchInputHandler = this.searchInputHandler.bind(this)
        this.followHandler = this.followHandler.bind(this)
    }
    render() {
        return (
            <div className={`${styles.searchContainer} ${styles.center}`}>
                <div className={styles.inputContainer}>
                    <input placeholder='Buscar un usuario' type='input' onChange={this.searchInputHandler} value={this.state.searchInput} />
                    <input value='Buscar' type='button' onClick={this.searchHandler} />
                </div>
                {this.state.foundUserNickname && <input value='Seguir' type='button' onClick={this.followHandler} />}
                {this.state.foundUserNickname && <TweetList nickname={this.state.foundUserNickname} tweetDisabled />}
            </div>
        )
    }

}