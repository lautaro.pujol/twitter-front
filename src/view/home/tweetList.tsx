import React, { ChangeEvent, Component } from 'react'
import { RouteComponentProps } from 'react-router-dom';
import UserActions from '../../actions/user.actions';
import styles from './style.module.css'

type Props = {
    nickname: string,
    tweetDisabled?: boolean
}
type State = {
    tweets: string[],
    getTweets: (nickname: string) => Promise<string[]>,
    addTweet: (nickname: string, tweet: string) => Promise<void>,
    tweet: string,
}

export default class TweetList extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            tweets: [],
            getTweets: new UserActions().getTweets,
            tweet: '',
            addTweet: new UserActions().tweet,
        };
        this.clickHandler = this.clickHandler.bind(this)
        this.tweetHandler = this.tweetHandler.bind(this);
        this.updateTweets = this.updateTweets.bind(this);
    }
    tweetHandler(event: ChangeEvent<HTMLInputElement>) {
        this.setState({ tweet: event.target.value })
    }
    clickHandler() {
        this.state.addTweet(this.props.nickname, this.state.tweet).then(this.updateTweets)
    };
    componentDidMount() {
        this.updateTweets()
    }
    updateTweets() {
        this.state.getTweets(this.props.nickname).then((tweets) => {
            this.setState({ tweets: tweets, tweet: '' })
        })
    }
    render() {
        return (
            <div className={styles.tweesList}>
                {this.state.tweets.map((tweet => <div className={styles.tweet}>
                    <span>{tweet}</span>
                    <div className={styles.divider} />
                </div>))}
                {
                    !this.props.tweetDisabled && <div className={styles.inputContainer}>
                        <input placeholder='Escriba un tweet' type='input' onChange={this.tweetHandler} value={this.state.tweet} />
                        <input value='Subir' type='button' onClick={this.clickHandler} />
                    </div>
                }
            </div>
        )
    }
}
