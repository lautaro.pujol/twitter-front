import React, { Component } from 'react'
import { RouteComponentProps } from 'react-router-dom';
import User from '../../model/user';
import styles from './style.module.css'
import TweetList from './tweetList'
import UserSearchComponent from './userSearchComponent'

type State = {
    user?: User
}

export default class HomeView extends Component<RouteComponentProps<any>, State> {
    constructor(props: RouteComponentProps<any>) {
        super(props);
        this.state = {
            user: undefined,
        };
    }
    componentDidMount() {
        const user: User = (this.props.location.state as any).user
        this.setState({ user: user })
    }
    render() {
        return (
            <div className={`${styles.homeContainer}`}>
                {this.state.user && <TweetList nickname={this.state.user.nickname} />}
                {this.state.user && <UserSearchComponent nickname={this.state.user.nickname} />}
            </div>
        )
    }

}