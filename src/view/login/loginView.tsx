import React, { Component } from 'react'
import styles from './style.module.css'
import RegisterForm from './registerForm'
import LoginForm from './loginForm'
import { RouteComponentProps } from 'react-router-dom'
export default class LoginView extends Component<RouteComponentProps<any>> {
    render() {
        return (
            <div className={`${styles.loginContainer} ${styles.center}`}>
                <RegisterForm/>
                <LoginForm {...this.props}/>
            </div>
        )
    }
}