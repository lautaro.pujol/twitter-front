import React, { ChangeEvent, Component, FormEvent } from 'react'
import styles from './style.module.css'
import UserActions from '../../actions/user.actions'

type State = {
    name: string,
    nickname: string,
    register: (name: string, nickname: string) => Promise<void>
}

export default class RegisterForm extends Component<{}, State> {
    handleRegister() {
        this.state.register(this.state.nickname, this.state.name)
    };
    handleNicknameInput(event: ChangeEvent<HTMLInputElement>) {
        this.setState((prevState) => ({ ...prevState, nickname: event.target.value }))
    };
    handleNameInput(event: ChangeEvent<HTMLInputElement>) {
        this.setState((prevState) => ({ ...prevState, name: event.target.value }))
    };
    constructor(props: {} | Readonly<{}>) {
        super(props);
        this.state = {
            name: '',
            nickname: '',
            register: new UserActions().register
        };

        this.handleRegister = this.handleRegister.bind(this);
        this.handleNicknameInput = this.handleNicknameInput.bind(this);
        this.handleNameInput = this.handleNameInput.bind(this);
    }
    render() {
        return (
            <div className={styles.inputContainer}>
                <input className={styles.input} type="text" placeholder='Name' onChange={this.handleNameInput} value={this.state.name} />
                <input className={styles.input} type="text" placeholder='Nickname' onChange={this.handleNicknameInput} value={this.state.nickname} />
                <input className={styles.input} type="button" value="Registrar" onClick={this.handleRegister}/>
            </div>
        )
    }
}