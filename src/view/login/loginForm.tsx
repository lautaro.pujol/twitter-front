import React, { ChangeEvent, Component, FormEvent } from 'react'
import { RouteComponentProps } from 'react-router-dom';
import UserActions from '../../actions/user.actions';
import User from '../../model/user';
import styles from './style.module.css'

type State = {
    nickname: string,
    login: (nickname: string) => Promise<User | undefined>
}

export default class LoginForm extends Component<RouteComponentProps<any>, State> {
    handleLogin() {
        this.state.login(this.state.nickname).then((user) => {
            if (user)
                this.props.history.push('home', { user: user })
        })
    };
    handleNicknameInput(event: ChangeEvent<HTMLInputElement>) {
        this.setState((prevState) => ({ ...prevState, nickname: event.target.value }))
    };
    constructor(props: RouteComponentProps<any>) {
        super(props);
        this.state = {
            nickname: '',
            login: new UserActions().login
        };
        this.handleNicknameInput = this.handleNicknameInput.bind(this)
        this.handleLogin = this.handleLogin.bind(this);
    }
    render() {
        return (
            <div className={styles.inputContainer} >
                <input className={styles.input} type="text" placeholder='Nickname' onChange={this.handleNicknameInput} value={this.state.nickname} />
                <input className={styles.input} type="submit" value="Ingresar" onClick={this.handleLogin} />
            </div>
        )
    }
}