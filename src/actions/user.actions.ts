import UserApi from '../api/userApi'

export default class UserActions {
    protected userApi: UserApi
    constructor() {
        this.userApi = new UserApi()
        this.register = this.register.bind(this)
        this.getTweets = this.getTweets.bind(this)
        this.addFollower = this.addFollower.bind(this)
        this.tweet = this.tweet.bind(this)
        this.login = this.login.bind(this)
    }
    async register(nickname: string, name: string) {
        try {
            await this.userApi.register({
                nickname: nickname,
                fullName: name
            })
        }
        catch (error) {
            // alert(error)
        }
    }
    async login(nickname: string) {
        try {
            console.log('==============');
            return await this.userApi.login({
                nickname: nickname,
            }).then(response => {
                return response.data
            })
        }
        catch (error) {
            console.error(error);
        }
    }
    async addFollower(nicknameToFollow: string, followerNickname: string) {
        await this.userApi.addFollower({
            nicknameToFollow: nicknameToFollow,
            followerNickname: followerNickname
        })
    }
    async tweet(nickname: string, tweet: string) {
        await this.userApi.tweet({
            nickname: nickname,
            tweet: tweet
        })
    }

    async getTweets(nickname: string) {
        return this.userApi.getTweets({
            nickname: nickname,
        }).then(response => {
            return response.data
        })
    }

}

