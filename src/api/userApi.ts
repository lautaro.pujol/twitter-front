import Axios from 'axios'
import User from '../model/user'

const baseUrl = 'http://localhost:8000/user/'
Axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*"
export default class UserApi {
    async login(loginData: LoginData) {
        console.log('88888888');
        return Axios.get<User>(baseUrl + 'login', {
            params: loginData,
        })
    }
    async getTweets(getTweetsData: GetTweetsFromUserData) {
        return Axios.get<string[]>(baseUrl + 'getTweets', {
            params: getTweetsData,
        })
    }
    async tweet(tweetData: TweetData) {
        await Axios.post(baseUrl + 'tweet', tweetData)
    }
    async addFollower(addFollowerData: AddFollowerData) {
        await Axios.post(baseUrl + 'addFollower', addFollowerData)

    }
    async register(registerData: UserRegisterData) {
        await Axios.post(baseUrl + 'register', registerData)
    }
}
export type UserRegisterData = {
    nickname: string;
    fullName: string;
}
export type AddFollowerData = {
    followerNickname: string,
    nicknameToFollow: string
}
export type TweetData = {
    nickname: string,
    tweet: string
}
export interface GetTweetsFromUserData {
    nickname: string,
}
export interface LoginData {
    nickname: string
}