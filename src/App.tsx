import React from 'react';
import LoginView from './view/login/loginView';
import HomeView from './view/home/homeView';
import './App.css';
import { Route, Router } from 'react-router-dom';
import { createBrowserHistory } from "history";

const history = createBrowserHistory();
function App() {
  return (
    <div className='App'>
      <Router history={history} >
        <Route exact path="/" component={LoginView} />
        <Route exact path="/home" component={HomeView} />
      </Router>
    </div>
    // <div className="App">
    //   <form>
    //     <label>
    //       Name:
    // <input type="text" name="name" onChange/>
    //     </label>
    //     <input type="submit" value="Submit" />
    //   </form>
    // </div>
  );
}

export default App;
