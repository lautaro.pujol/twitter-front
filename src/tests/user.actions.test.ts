import UserActions from "../actions/user.actions";
import UserApi, { AddFollowerData, UserRegisterData, TweetData, GetTweetsFromUserData } from "../api/userApi";
import { AxiosResponse } from 'axios'
class FakeUserActions extends UserActions {


    constructor() {
        super()
        super.userApi = {} as UserApi
    }
    public userApi = super.userApi
}

describe('userActions register should', () => {
    let userActions: FakeUserActions
    const FULL_NAME: string = 'NAME'
    const NICKNAME: string = 'NICKNAME'
    const LOGIN_DATA: UserRegisterData = {
        fullName: FULL_NAME,
        nickname: NICKNAME
    }
    beforeEach(() => {
        userActions = new FakeUserActions()
    })
    it('register a user through userApi', async (): Promise<void> => {
        const registerApi = jest.fn()
        userActions.userApi.register = registerApi
        await userActions.register(NICKNAME, FULL_NAME)
        expect(registerApi).toBeCalledWith(LOGIN_DATA)
    })
});


describe('userActions addFollower should', () => {
    let userActions: FakeUserActions
    const NICKNAME_TO_FOLLOW: string = 'NICKNAME_TO_FOLLOW'
    const FOLLOWER_NICKNAME: string = 'FOLLOWER_NICKNAME'
    const ADD_FOLLOWER_DATA: AddFollowerData = {
        followerNickname: FOLLOWER_NICKNAME,
        nicknameToFollow: NICKNAME_TO_FOLLOW
    }
    beforeEach(() => {
        userActions = new FakeUserActions()
    })
    it('add a follower through userApi', async (): Promise<void> => {
        const addFollowerApi = jest.fn()
        userActions.userApi.addFollower = addFollowerApi
        await userActions.addFollower(NICKNAME_TO_FOLLOW, FOLLOWER_NICKNAME)
        expect(addFollowerApi).toBeCalledWith(ADD_FOLLOWER_DATA)
    })
});


describe('userActions tweet should', () => {
    let userActions: FakeUserActions
    const NICKNAME: string = 'NICKNAME'
    const TWEET: string = 'this is a tweet'
    const TWEET_DATA: TweetData = {
        nickname: NICKNAME,
        tweet: TWEET
    }
    beforeEach(() => {
        userActions = new FakeUserActions()
    })
    it('add a follower through userApi', async (): Promise<void> => {
        const tweetApi = jest.fn()
        userActions.userApi.tweet = tweetApi
        await userActions.tweet(NICKNAME, TWEET)
        expect(tweetApi).toBeCalledWith(TWEET_DATA)
    })
});

describe('userActions get tweets should', () => {
    let userActions: FakeUserActions
    const NICKNAME: string = 'NICKNAME'
    const TWEET: string = 'this is a tweet'
    const axiosResponse = {
        data: [TWEET]
    } as AxiosResponse
    const GET_TWEET_DATA: GetTweetsFromUserData = {
        nickname: NICKNAME,
    }
    beforeEach(() => {
        userActions = new FakeUserActions()
    })
    it('add a follower through userApi', async (): Promise<void> => {
        const getTweets = jest.fn()
        userActions.userApi.getTweets = getTweets
        getTweets.mockReturnValue(Promise.resolve([]))
        const tweets = await userActions.getTweets(NICKNAME)
        expect(getTweets).toBeCalledWith(GET_TWEET_DATA)
    })
    it('return an array of tweets', async (): Promise<void> => {
        const getTweets = jest.fn()
        userActions.userApi.getTweets = getTweets
        getTweets.mockReturnValue(Promise.resolve(axiosResponse))
        expect(await userActions.getTweets(NICKNAME)).toEqual([TWEET])
    })
});
