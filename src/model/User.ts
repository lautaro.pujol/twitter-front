export default interface User {
    tweets: string[];
    nickname: string;
    fullName: string;
    folledUsers: Set<string>
}